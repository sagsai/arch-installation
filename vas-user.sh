#!/usr/bin/env bash

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
# BIBlack='\033[1;90m'      # Black
# BIRed='\033[1;91m'        # Red
RED='\033[0;31m'
# BIGreen='\033[1;92m'      # Green
GREEN='\033[0;32m'
# BIYellow='\033[1;93m'     # Yellow
# Yellow='\033[1;33'
# BIBlue='\033[1;94m'       # Blue
# BIPurple='\033[1;95m'     # Purple
# BICyan='\033[1;96m'       # Cyan
CYAN='\033[0;36m'
NC='\033[0m'

cd ~
mkdir -p ~/.config
mkdir -p ~/Screenshots
mkdir -p ~/Pictures
mkdir -p ~/Wallpapers
mkdir -p ~/Downloads
mkdir -p ~/.local/bin
mkdir -p ~/.local/lib
mkdir -p ~/.local/share/fonts/
mkdir -p ~/.local/src
mkdir -p ~/.local/state

WORK_DIR="$HOME/Downloads"
LOG_FILE="$WORK_DIR/installation.log"

# Update pacman database before installing anything
sudo pacman -Syy
sudo pacman -Syu

echo -e "${CYAN}Installing a lot of stuff, will take some time, so sit back & relax but keep an eye on the screen!${NC}"
sudo pacman -S --needed --noconfirm nmap openssh git ntp zsh wget whois | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm man-db man-pages | tee -a "$LOG_FILE"

sudo pacman -S --needed --noconfirm rust | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm go | tee -a "$LOG_FILE"

sudo pacman -S --needed --noconfirm bind-tools | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm xf86-video-intel | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm polkit | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm python python-pip python-pipx python-virtualenv | tee -a "$LOG_FILE"
# sudo pacman -S --needed --noconfirm xorg | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm xorg-server | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm xorg-xwininfo | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm xorg-xinit | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm xorg-xprop | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm xorg-xdpyinfo | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm xorg-xbacklight | tee -a "$LOG_FILE"
# wallpaper setting utility for X
sudo pacman -S --needed --noconfirm xwallpaper | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm dialog xdialog | tee -a "$LOG_FILE"
# hides your X mouse cursor when not needed
sudo pacman -S --needed --noconfirm unclutter | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm unzip | tee -a "$LOG_FILE"
# allows you to use a modifier key as another key when pressed and released on its own
sudo pacman -S --needed --noconfirm xcape | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm xclip | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm xsel | tee -a "$LOG_FILE"
# simulate keyboard input and mouse activity
sudo pacman -S --needed --noconfirm xdotool | tee -a "$LOG_FILE"
# X composite manager to show shadows and stuff
sudo pacman -S --needed --noconfirm xcompmgr | tee -a "$LOG_FILE"
# Simple X Image Viewer
sudo pacman -S --needed --noconfirm sxiv | tee -a "$LOG_FILE"
# image viewer & wallpaper setter
sudo pacman -S --needed --noconfirm feh | tee -a "$LOG_FILE"
# Command to show images on the terminal
sudo pacman -S --needed --noconfirm ueberzug | tee -a "$LOG_FILE"
# Screenshot
sudo pacman -S --needed --noconfirm maim | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm ttf-linux-libertine | tee -a "$LOG_FILE"
# command line precision calculator
sudo pacman -S --needed --noconfirm bc | tee -a "$LOG_FILE"
# visual front end for XRandR to set monitor positions
sudo pacman -S --needed --noconfirm arandr | tee -a "$LOG_FILE"
# FUSE filesystem that supports reading/writing from MTP devices(e.g. android)
sudo pacman -S --needed --noconfirm mtpfs | tee -a "$LOG_FILE"
# Desktop notifications
sudo pacman -S --needed --noconfirm libnotify | tee -a "$LOG_FILE"
# Lightweight and customizable notification daemon
sudo pacman -S --needed --noconfirm dunst | tee -a "$LOG_FILE"

# process multimedia content
sudo pacman -S --needed --noconfirm ffmpeg | tee -a "$LOG_FILE"
# video thumbnailer
sudo pacman -S --needed --noconfirm ffmpegthumbnailer | tee -a "$LOG_FILE"
# music player daemon
sudo pacman -S --needed --noconfirm mpd | tee -a "$LOG_FILE"
# Minimalist command line interface to MPD
sudo pacman -S --needed --noconfirm mpc | tee -a "$LOG_FILE"
# media player
sudo pacman -S --needed --noconfirm mpv | tee -a "$LOG_FILE"
# mpd client
sudo pacman -S --needed --noconfirm ncmpcpp | tee -a "$LOG_FILE"

# @TODO after os installation install the audio server
# Capture audio & video & other handling options
# sudo pacman -S --needed --noconfirm pipewire | tee -a "$LOG_FILE"
# sudo pacman -S --needed --noconfirm pipewire-audio | tee -a "$LOG_FILE"
# sudo pacman -S --needed --noconfirm pipewire-pulse | tee -a "$LOG_FILE"
# sudo pacman -S --needed --noconfirm pipewire-alsa | tee -a "$LOG_FILE"
# sudo pacman -S --needed --noconfirm wireplumber | tee -a "$LOG_FILE"
# sudo pacman -S --needed --noconfirm pwvucontrol | tee -a "$LOG_FILE"
# systemctl --user enable pipewire.service
# systemctl --user start pipewire.service
# systemctl --user enable pipwire-pulse.service
# systemctl --user start pipwire-pulse.service

# sudo pacman -S --needed --noconfirm pulsemixer | tee -a "$LOG_FILE"
# sudo pacman -S --needed --noconfirm pamixer | tee -a "$LOG_FILE"
# sudo pacman -S --needed --noconfirm pavucontrol | tee -a "$LOG_FILE"
# sudo pacman -S --needed --noconfirm alsa-utils | tee -a "$LOG_FILE"

sudo pacman -S --needed --noconfirm zathura | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm zathura-pdf-mupdf | tee -a "$LOG_FILE"
# PDF rendering library based on xpdf
sudo pacman -S --needed --noconfirm poppler | tee -a "$LOG_FILE"
# Supplies technical and tag information about a video or audio file
sudo pacman -S --needed --noconfirm mediainfo | tee -a "$LOG_FILE"
# script for managing file archives of various types
sudo pacman -S --needed --noconfirm atool | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm fzf | tee -a "$LOG_FILE"
# Cat clone with syntax highlighting and git integration
sudo pacman -S --needed --noconfirm bat | tee -a "$LOG_FILE"
# Simple X display locker
sudo pacman -S --needed --noconfirm slock | tee -a "$LOG_FILE"
# Multipurpose relay
sudo pacman -S --needed --noconfirm socat | tee -a "$LOG_FILE"
# collection of the unix tools
sudo pacman -S --needed --noconfirm moreutils | tee -a "$LOG_FILE"
# tldr client in rust
sudo pacman -S --needed --noconfirm tealdeer | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm btop neofetch | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm pacman-contrib | tee -a "$LOG_FILE"
# terminal based calendar, need to figure out
# sudo pacman -S --needed --noconfirm calcurse | tee -a "$LOG_FILE"
# command line calendar, not very useful
# sudo pacman -S --needed --noconfirm when | tee -a "$LOG_FILE"
# Useful time tracking command line app
# sudo pacman -S --needed --noconfirm timew | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm zenity yad | tee -a "$LOG_FILE"

sudo pacman -S --needed --noconfirm nodejs | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm npm | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm ripgrep | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm fd | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm the_silver_searcher | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm ranger | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm trash-cli | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm zoxide | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm python-pdftotext | tee -a "$LOG_FILE"
# Does not work
# sudo pacman -S --needed --noconfirm sshfs | tee -a "$LOG_FILE"

# browsers
# sudo pacman -S --needed --noconfirm tor torbrowser-launcher | tee -a "$LOG_FILE"
# sudo pacman -S --needed --noconfirm lynx w3m httpie python-adblock qutebrowser firefox | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm lynx w3m httpie python-adblock firefox | tee -a "$LOG_FILE"
sudo pacman -S --needed --noconfirm chromium | tee -a "$LOG_FILE"
# Required for playing paid(DRM) content on qutebrowser
# yay -S chromium-widevine
# vimb
# terminals
# @NOTE rxvt-unicode-truecolor-wide-glyphs is installed instead of rxvt-unicode
sudo pacman -S --needed --noconfirm xterm alacritty terminator kitty | tee -a "$LOG_FILE"
# install kitty-terminfo if not installed automatically
# sudo pacman -S --needed --noconfirm w3m w3m-img
sudo pacman -S --needed --noconfirm i3-wm rofi polybar | tee -a "$LOG_FILE"

# check if the following ones are required & if not installing them causes any problems
# sudo pacman -S --needed --noconfirm newsboat				# RSS feed reader
# sudo pacman -S --needed --noconfirm ntfs-3g				# read & write to ntfs drives
# sudo pacman -S --needed --noconfirm gnome-keyring			# check not installing it causes any problems
# sudo pacman -S --needed --noconfirm noto-fonts-emoji		# check if required
# sudo pacman -S --needed --noconfirm dosfstools			# mkfs.fat, fsck.fat and fatlabel to create, check and label file systems of the FAT family
# sudo pacman -S --needed --noconfirm exfat-utils			# Free exFAT file system implementation
# sudo pacman -S --needed --noconfirm python-qdarkstyle		# dark light style sheet for Qt applications
# sudo pacman -S --needed --noconfirm bpython				# Fancy ncurses interface to the Python interpreter
# sudo pacman -S --needed --noconfirm tmux
sudo pacman -S --needed --noconfirm yt-dlp					# youtube-dl fork with additional features(required for playing yt videos using mpv)
sudo pacman -S --needed --noconfirm libreoffice-still		# stable maintenace branch of libreoffice
sudo pacman -S --needed --noconfirm meld					# graphical diff & merge tool

sudo npm install -g npm-check-updates | tee -a "$LOG_FILE"
sudo npm install -g grunt-cli | tee -a "$LOG_FILE"
sudo npm install -g speed-test | tee -a "$LOG_FILE"
# For taking screenshots from command line
# sudo npm install -g puppeteer | tee -a "$LOG_FILE"
# sudo npm install -g capture-website-cli | tee -a "$LOG_FILE"
# sudo npm install -g pageres-cli | tee -a "$LOG_FILE"

# sudo npm install -g jshint
sudo npm install -g http-server

# No need to do that in a virtual environment
# Enable accessing the system from outside over https & http
# sudo ufw default deny
# sudo ufw allow from 192.168.0.0/24
# sudo ufw allow in 443/tcp
# sudo ufw allow in 80/tcp
# sudo ufw status numbered
# sudo ufw enable

mandb

# will take effect from the next session
localctl set-x11-keymap us colemak

# change shell to zsh
chsh -s /bin/zsh "$USER"

# make zsh history file
mkdir -p ~/.cache/zsh
touch ~/.cache/zsh/history

# make & install yay for aur
mkdir -p "$HOME/.local/src"

# don't want to have to ssh-add the key every session before git commit
wget -O ~/.local/bin/ssh https://raw.githubusercontent.com/ssh-ident/ssh-ident1/dev/ssh-ident
chmod 0755 ~/.local/bin/ssh

echo -e "${CYAN}Installing yay${NC}"
git clone https://aur.archlinux.org/yay.git	"$WORK_DIR/yay"
if cd "$WORK_DIR/yay"; then
	makepkg -si
	cd .. && sudo rm -R "$WORK_DIR/yay"
else
	echo -e "${RED}COULD NOT INSTALL YAY, EXITING!${NC}"
	exit 1
fi

echo -e "${CYAN}Installing some required packages from AUR${NC}"
# yay -S --needed --noconfirm rxvt-unicode-pixbuf | tee -a "$LOG_FILE"
# the following version of urxvt shows font icons another alternative is rxvt-unicode-cvs-patched-wideglyphs
yay -S --needed --noconfirm rxvt-unicode-truecolor-wide-glyphs urxvt-tabbedex | tee -a "$LOG_FILE"
# yay -S --needed --noconfirm librewolf-bin | tee -a "$LOG_FILE"
# FreeType-based font drawing library for X
yay -S --needed --noconfirm libxft-git | tee -a "$LOG_FILE"
# lf is a terminal file manager written in Go inspired by ranger
# yay -S --needed --noconfirm lf-git | tee -a "$LOG_FILE"
# A flat, grey gruvboxed theme with transparent elements for GTK 3, GTK 2 and gnome-shell
yay -S --needed --noconfirm gtk-theme-arc-gruvbox-git | tee -a "$LOG_FILE"
# Google Noto fonts
sudo pacman -S --needed --noconfirm noto-fonts | tee -a "$LOG_FILE"
# Text-based addressbook designed for use with Mutt
# yay -S --needed --noconfirm abook | tee -a "$LOG_FILE"
# Optimized and extended zsh-syntax-highlighting
yay -S --needed --noconfirm zsh-fast-syntax-highlighting | tee -a "$LOG_FILE"
# Queue up tasks from the shell for batch execution
yay -S --needed --noconfirm task-spooler | tee -a "$LOG_FILE"
# FUSE filesystem that supports reading/writing from MTP devices(e.g. android)
yay -S --needed --noconfirm simple-mtpfs | tee -a "$LOG_FILE"
yay -S --needed --noconfirm dragon-drop | tee -a "$LOG_FILE"
yay -S --needed --noconfirm colorpicker-ym1234-git | tee -a "$LOG_FILE"
# yay -S --needed --noconfirm sc-im
# yay -S --needed --noconfirm htop-vim

# fix python attributeerror module 'attr' has no attribute 's'
sudo pip uninstall attr | tee -a "$LOG_FILE"
sudo pip uninstall attrs | tee -a "$LOG_FILE"
pip install attrs | tee -a "$LOG_FILE"
pipx install powerline-shell | tee -a "$LOG_FILE"

# installing nerd fonts(install the font hack)
# @NOTE the installed fonts should be inside ~/.local/share/fonts
yay -S --needed --noconfirm getnf

# showing i3wm keybindings on a popup window
yay -S --needed --noconfirm i3help-git

sudo pacman -S --needed --noconfirm dictd aspell | tee -a "$LOG_FILE"

yay -S --needed --noconfirm skypeforlinux-bin | tee -a "$LOG_FILE"
yay -S --needed --noconfirm recutils | tee -a "$LOG_FILE"

echo -e "${GREEN}Install design tools e.g. gimp, inkscape etc?(y/N)${NC}"
sleep 2
read installdesigntools
if [ "$installdesigntools" == 'y' ]; then
	sudo pacman -S --needed --noconfirm imagemagick gimp inkscape | tee -a "$LOG_FILE"
	# svg optimizer
	sudo pacman -S --needed --noconfirm scour | tee -a "$LOG_FILE"
	sudo npm -g install svgo | tee -a "$LOG_FILE"
fi

echo -e "${GREEN}Install php, composer & mariadb-client?(y/N)${NC}"
sleep 2
read installserver
if [ "$installserver" == 'y' ]; then
	sudo pacman -S --needed --noconfirm php composer | tee -a "$LOG_FILE"
	sudo pacman -S --needed --noconfirm mariadb-clients | tee -a "$LOG_FILE"
fi

# virtualbox guest plugins
# sudo pacman --needed --noconfirm -S vagrant
# vagrant plugin install vagrant-vbguest

alias pip="pipx"
echo -e "${GREEN}Install Lunarvim & its dependencies?(y/N)${NC}"
sleep 2
read installlvim
if [ "$installlvim" == 'y' ]; then
	# Resolve EACCES permissions when installing packages globally using npm
	mkdir ~/.npm-global
	npm config set prefix '~/.npm-global'
	echo 'export PATH="$HOME/.npm-global/bin:$PATH"' >> .config/zsh/.zshrc

	sudo pacman -S --needed --noconfirm neovim | tee -a "$LOG_FILE"
	sudo pacman -S --needed --noconfirm lazygit | tee -a "$LOG_FILE"

	# pip install pynvim
	pip install sqlfluff | tee -a "$LOG_FILE"
	pip install pylint | tee -a "$LOG_FILE"
	pip install black | tee -a "$LOG_FILE"

# 	sudo pacman -S --needed --noconfirm luarocks | tee -a "$LOG_FILE"
# 	sudo pacman -S --needed --noconfirm python-pynvim | tee -a "$LOG_FILE"
# 	sudo pacman -S --needed --noconfirm re2 | tee -a "$LOG_FILE"
# 	sudo pacman -S --needed --noconfirm cpanminus | tee -a "$LOG_FILE"

# 	# code formatter, no control over how it works
# 	# sudo pacman -S --needed --noconfirm shellharden | tee -a "$LOG_FILE"
# 	# Code linter, can ignore whenever appropriate
	sudo pacman -S --needed --noconfirm shellcheck | tee -a "$LOG_FILE"
	sudo pacman -S --needed --noconfirm tidy | tee -a "$LOG_FILE"
	npm install -g sql-formatter | tee -a "$LOG_FILE"
	npm install -g jsonlint | tee -a "$LOG_FILE"
	npm install -g eslint_d | tee -a "$LOG_FILE"
	npm install -g stylelint | tee -a "$LOG_FILE"

# 	sudo cpanm -n Neovim::Ext | tee -a "$LOG_FILE"
# 	sudo luarocks install luasocket | tee -a "$LOG_FILE"
# 	sudo luarocks install pcre2 | tee -a "$LOG_FILE"
# 	sudo npm install -g stylelint-config-standard | tee -a "$LOG_FILE"
# 	pip install pyre2 | tee -a "$LOG_FILE"
# 	sudo pacman -S --needed --noconfirm python-pylint | tee -a "$LOG_FILE"
# 	# pip install yapf | tee -a "$LOG_FILE"

	bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install.sh)

# 	cpanm --local-lib=~/perl5 local::lib && eval "$(perl -I ~/perl5/lib/perl5/ -Mlocal::lib)"
fi

mv ~/.config ~/config-bak
git clone https://gitlab.com/sagsai/arch-config ~/.config

echo -e "${CYAN}Setting up the config files & installing ranger & rofi plugins${NC}"
# Now that the .config dir has been copied install the lvim, ranger & rofi plugins
mkdir -p ~/.config/nvim/sessions
git clone https://github.com/alexanderjeurissen/ranger_devicons 	~/.config/ranger/plugins/ranger_devicons
git clone https://github.com/jchook/ranger-zoxide 					~/.config/ranger/plugins/zoxide
git clone https://github.com/maximtrp/ranger-archives.git 			~/.config/ranger/plugins/ranger-archives
git clone https://github.com/adi1090x/rofi.git 						"$WORK_DIR/rofi"
if cd "$WORK_DIR/rofi"; then
	chmod +x setup.sh
	./setup.sh
	cd .. && sudo rm -R "$WORK_DIR/rofi"
fi
mv ~/config-bak/lvim/plugin ~/.config/lvim
# @TODO after installation see what this does(https://github.com/adi1090x/rofi)
# mv "$HOME/.config/rofi.$USER/config.rasi" "$HOME/.config/rofi.$USER"/.gitignore ~/.config/rofi

# make the appropriate links
ln -s ~/.config/gtk-2.0/gtkrc-2.0 ~/.gtkrc-2.0
ln -s ~/.config/shell/inputrc ~/.inputrc
ln -s ~/.config/msmtp/config ~/.msmtprc
ln -s ~/.config/.taskbook.json ~/.taskbook.json
# ln -s ~/.config/x11/.Xdefaults ~/.Xdefaults
ln -s ~/.config/x11/xinitrc ~/.xinitrc
ln -s ~/.config/x11/xmodmap ~/.Xmodmap
ln -s ~/.config/x11/xprofile ~/.xprofile
ln -s ~/.config/x11/xresources ~/.Xresources
ln -s ~/.config/shell/profile ~/.zprofile

echo -e "${CYAN}Setting up git globally for $USER${NC}"
# Git configuration
reuse_email="n"
echo -e "${GREEN}Enter name for git:${NC}"
sleep 2
read name
if [ "$name" != "" ]; then
	git config --global user.name "$name"
fi
echo -e "${GREEN}Enter email for git globally or leave empty:${NC}"
sleep 2
read email
if [ "$email" != "" ]; then
	git config --global user.email "$email"

	echo -e "${GREEN}Make SSH key for the same email for communicating with gitlab?(y/n)${NC}"
    sleep 2
	read reuse_email
fi

if [[ "$email" == '' || "$reuse_email" != 'y' ]]; then
	echo -e "${GREEN}Enter email for making ssl certificate for gitlab or leave empty:${NC}"
    sleep 2
	read email
fi

if [ "$email" != "" ]; then
	echo -e "${CYAN}Creating ssh key for communicating with gitlab${NC}"
	# Add ssh key for communicating to gitlab without
	ssh-keygen -t ed25519 -C "$email"
	xclip -sel clip < ~/.ssh/id_ed25519.pub
	# check that the key is copied
	ssh-add ~/.ssh/id_ed25519
	ssh-add -l
	# At this point copy the public key in gitlab at https://gitlab.com/-/profile/keys
	ssh -T git@gitlab.com
fi

# @TODO @TEST firefox/inkscape may have installed a font package that messes up rendering of the braille character(lvim dashboard & btop)
# sudo pacman -R --needed --noconfirm gnu-free-fonts

# project tracker on polybar
(crontab -l 2>/dev/null; echo "* * * * * ~/.config/polybar/track >> /dev/null 2>&1") | sort - | uniq - | crontab -
# Add a crontab entry to update the dns entry for subdomains

# Remove the script from the .bashrc so that it does not run after login again
sed -i '/bash <(curl -Ls https:\/\/gitlab.com\/sagsai/d' ~/.bashrc

echo -e "${CYAN}Installation done, rebooting so that all the changes are effective"
sleep 2
sudo reboot now

exit
