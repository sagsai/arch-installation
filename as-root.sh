#!/usr/bin/env bash

# TODO save all the messages in a log file

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
# BIBlack='\033[1;90m'      # Black
# BIRed='\033[1;91m'        # Red
# RED='\033[0;31m'
# BIGreen='\033[1;92m'      # Green
GREEN='\033[0;32m'
# BIYellow='\033[1;93m'     # Yellow
# Yellow='\033[1;33'
# BIBlue='\033[1;94m'       # Blue
# BIPurple='\033[1;95m'     # Purple
# BICyan='\033[1;96m'       # Cyan
CYAN='\033[0;36m'
NC='\033[0m'

# @WARN wont work if the partitions change or for another hdd
mkdir /mnt/stuff /mnt/work
echo "UUID=8b338308-aae6-4ff2-bd58-ac972416e4a4	/mnt/work	ext4		rw,relatime 0 2  " | tee -a /etc/fstab
echo "UUID=b69f6058-e0be-48aa-a785-4d66600fedf3	/mnt/stuff	ext4		rw,relatime 0 2  " | tee -a /etc/fstab

echo "127.0.0.1	localhost" | tee -a /etc/hosts

echo -e "${GREEN}Enter password for root:${NC}"
sleep 1
passwd

echo -e "${GREEN}Enter the username of the other user or leave empty:${NC}"
sleep 2
read name
if [ "$name" != '' ]; then
	useradd -mG wheel "$name"
	passwd "$name"
	# uncomment the line starting with wheel to enable them as sudoer
	# vim /etc/sudoers
	sed -i 's/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /etc/sudoers
	sed -i 's/# %sudo\tALL=(ALL:ALL) ALL/%sudo\tALL=(ALL:ALL) ALL/' /etc/sudoers
	echo "%wheel ALL=(ALL:ALL) NOPASSWD: /usr/bin/shutdown,/usr/bin/reboot,/usr/bin/systemctl suspend,/usr/bin/wifi-menu,/usr/bin/mount,/usr/bin/umount" > "/etc/sudoers.d/01-$name"
	# Make pacman colorful, concurrent downloads and Pacman eye-candy.
	grep -q "ILoveCandy" /etc/pacman.conf || sed -i "/#VerbosePkgLists/a ILoveCandy" /etc/pacman.conf
	sed -Ei "s/^#(ParallelDownloads).*/\1 = 5/;/^#Color$/s/#//" /etc/pacman.conf
	# Use all cores for compilation.
	sed -i "s/-j2/-j$(nproc)/;/^#MAKEFLAGS/s/^#//" /etc/makepkg.conf
fi

# TODO provision for setting other keymaps or leaving the default one intact
# to make sure that colemak works in tty
echo "KEYMAP=colemak" > /etc/vconsole.conf

# uncomment the preferred locales in the following file
# /en_US (then uncomment en_US.UTF-8 + ISO)
# TODO get the locale from user input
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
sed -i 's/#en_US ISO-8859-1/en_US ISO-8859-1/' /etc/locale.gen
# vim /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

echo -e "${GREEN}Enter hostname:${NC}"
sleep 2
read hostname
if [ "$hostname" != '' ]; then
	echo "lappy" >> /etc/hostname
fi
# or type the name of the pc name after editing it
# vim /etc/hostname

# TODO get the timezone from user input
ln -sf /usr/share/zoneinfo/Asia/Calcutta /etc/localtime

pacman -S --needed --noconfirm networkmanager
systemctl enable NetworkManager

pacman -S --needed --noconfirm grub
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

# Install & enable firewall service
pacman -S --needed --noconfirm ufw
systemctl enable ufw.service
# systemctl start ufw

pacman -S --needed --noconfirm cronie
systemctl enable cronie.service

hwclock --systohc

if [ "$name" != '' ]; then
	# TODO change the .bashrc to download & execute a script on the next login by the specific user only once
	# su - "$name" -c "bash <(curl -Ls https://gitlab.com/sagsai/arch-installation/-/raw/master/as-user.sh)"
	echo "bash <(curl -Ls https://gitlab.com/sagsai/arch-installation/-/raw/master/as-user.sh)" >> "/home/$name/.bashrc"
fi

exit

