#!/usr/bin/env bash

# TODO provision for setting other keymaps or leaving the default one intact
# make colemak work
loadkeys colemak

# make sure connected to some network
ip link
ping -c 2 archlinux.org

# if connecting over wifi while installing over actual hardware
# iwctl

pacman -Syy
# check the disk disk & partitions
# lsblk

# make partitions and format them
# no need to create separate partitions for / & /boot just set the boot(b) flag to the created partition
cfdisk /dev/sda
mkfs.ext4 /dev/sda1
# mkfs.ext4 /dev/sda2
mount /dev/sda1 /mnt
# mkdir /mnt/boot
# mount /dev/sda1 /mnt/boot
# lsblk

# install the linux kernel and the most important stuff
pacstrap -K /mnt base base-devel linux-lts linux-firmware linux-lts-headers nano vim intel-ucode
genfstab -U /mnt >> /mnt/etc/fstab

# os kernel installation done, now chroot into the fresh installation and install some stuff
arch-chroot /mnt /bin/bash <(curl -Ls https://gitlab.com/sagsai/arch-installation/-/raw/master/as-root.sh)

umount -R /mnt
# shutdown instead of rebootting since need take out the installation media
shutdown now
